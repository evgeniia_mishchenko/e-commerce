<?php
//typically controller name is plural
//and model - singular (CI naming convention)

//we need this controller be our default controller
//(so that base URL without name of this controller ) - config/routes
    class Products extends CI_Controller{
        public function index(){
            /*$data['name'] = 'Mike';*/
            //Load View (layout system)

            /*$this->load->view('products', $data); */
             //main layout and $data array so we can pass data into the view
             // and access it there like this - $name. Just like var.


            //Model is where classes gonna deal with database
            //So if you want to fetch sth from db in the products controller
            // then we need to reach into the products model, get it and pass to the products view
            // that's how MVC works


            //we will get it from the model but we eill store it in our data array
            // because that's what gonna be passed to our view

            /*
             * Get All Products
             */
            $data['products'] = $this->Product_model->get_products();   //and then we will use this returned array inside of our views
            /*
             * Load View
             */
            $data['main_content'] = 'products';
            $this->load->view('layouts/main', $data);  //and data will be our products view(or whatever else view)
            //just like this we have any view already with header/footer
        }

        public function details($id){
            //Get Product Details
            $data['product'] = $this->Product_model->get_product_details($id);
            //Load View
            $data['main_content'] = 'details';
            $this->load->view('layouts/main', $data);
        }
    }
?>