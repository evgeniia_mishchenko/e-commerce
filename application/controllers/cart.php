<?php

//name of this controller is singular because 'carts' doesnt make sense
 //https://ellislab.com/codeigniter/user-guide/libraries/cart.html

/*
 * first of all u load the library 'cart' (to autoload or $this->load->library('cart')
 * in order to use cart
 * and then Cart Object is available as $this->cart
 * U can add an Item to your cart ($data = array(id, qty, price..)
 * then pass this array by $this->cart-insert($data)) - it creates a session with a session array
 * and works with that behind the scene we only deal with the cart Object
 * To display u loop through it in View by: foreach($this->cart->contents() as $items)
 * to update cart: $this->cart->update($data)
 */

//this is gonna be quite a bit in this class its just what PayPal need for its API
// to be able to add any single individual product so u can login to paypal and see every product
// not just the total of your order
// U can set this in a configuration file
// its notgonna be advanced shopping cart - u just gonna have global tax and shipping amount
class Cart extends CI_Controller {

    public $paypal_dat = '';    //array for PayPal to use to be able to move on to the transaction on the PayPal site
    public $tax;
    public $shipping;
    public $total = 0;
    public $grand_total;

    /*
     *  Cart Index (to oad the cart View)
     */
    public function index (){
        //Load View
        $data['main_content'] = 'cart';
        $this->load->view('layouts/main', $data);
    }

    /*
     * Add To Cart
     */
    public function add(){
        //Item Data (grab it from the POST vars(hidden inputs which user submit))

        $data = array(
            'id' => $this->input->post('item_number'),    // with CI u dont use post superglobal $_POST['']:
            'qty' => $this->input->post('qty'),
            'price' => $this->input->post('price'),
            'name' => $this->input->post('title'),    //in DB this field is actually called title
            'image' => $this->input->post('image')
        );
        //print_r($data); die();  - to test

        //Insert Into Cart(easy)
        $this->cart->insert($data);
        redirect('products');   //and update cart area in the view
    }

    /*
     * Update Cart
     */
    /*public function update($in_cart = null){
        $data = $_POST;
        $this->cart->update($data);
        //ShowCart Page
        redirect('cart', 'refresh');
    }*/

}

