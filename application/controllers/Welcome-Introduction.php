<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()	//defaut function - what we see in URL(controller/function/parameters to this function
	{						//it is really easy to create website structure which is one of the great things about using framework
		$this->load->view('welcome_message');	//with load we load any view file
	}

	public function test(){		// URL will be - /welcome/test
		echo 'test';		//but we won't echo out anything from controller - view does it and from controller we want load View
	}
}
//what we do: we create a simple Layout system where we have some include files