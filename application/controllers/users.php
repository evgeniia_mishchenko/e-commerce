<?php

class Users extends CI_Controller {

    //users isnt gonna have index method (we dont need url/users - its not gonna do anything)
    //we need register function(url/users/register)
    public function register(){
        //Validation Rules
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[16]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'trim|required|matches[password]');

        //when we go to register page we wonna see if the form has been submitted or not
        //cause we dont wonna validate if nobody submitted the form
        if($this->form_validation->run() == FALSE){ //if the form hasnt submitted it will give us a view
            $data['main_content'] = 'register';
            $this->load->view('layouts/main', $data);
        } else {
            //since we've passed validation we need to go through User model and register the user
            if($this->User_model->register()){
                $this->session->set_flashdata('registered', 'You are now registered');  //dont forget to put it in view
                redirect('checkout');
            }
                //set_flashdata() is what CI uses so u can set a msg, redirect to a new page and still
                //have that msg to echo out onto the page. It uses sessions to do that
                //first param - name (of the flash data so u can grab onto this inside products where we gonna be redirecting to)
                //second - msg
        }

    // Form validation library -  any kind of form validation(for this to work we need form validation library and url helper(autoload))
    //(check required fields, check for valid email addresses or name longer than 8 chars and passwords match)
    // also in our view we wonna display errors - and all we need to add <?php echo validation_errors('<div class="alert alert-danger">', '</div>');

    //https://ellislab.com/codeigniter/user-guide/libraries/form_validation.html
    }

    public function login (){
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[16]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[50]');

        if($this->form_validation->run() == FALSE){
            $data['main_content'] = 'login';
            $this->load->view('layouts/main', $data);
        } else {
            $username = $this->input->post('username'); //the same as $_POST['username'] but CI is little more secure
            $password = md5($this->input->post('password'));

            $user_id = $this->User_model->login($username, $password);
            //Validate User
            if ($user_id) {
                //Create array of user data
                $data = array(
                    'user_id' => $user_id,
                    'username' => $username,
                    'logged_in' => true //this we will check if the user is actually logged in (eg if(!$this->session->userdata('logged_in'))
                );
                //Set session userdata
                $this->session->set_userdata($data);
                //Set message
                $this->session->set_flashdata('pass_login', 'You are logged in');
                redirect('checkout');
            } else {
                //Set error
                $this->session->set_flashdata('fail_login', 'Sorry, but username and password do not match or you do not have an account yet ');
                redirect('users/login');
            }
        }
    }

    public function logout (){
        //Unset user data
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
        $this->session->sess_destroy();

        redirect('products');
        //When you log out u destroy the session
        // and as your cart is also in a session so if u logged out your cart gets destroyed as well
        //which is normal functionality for most e-commerce sites
    }
}