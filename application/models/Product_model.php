<?php

    class Product_model extends CI_Model {

        //we need to load our Models just like libraries and helpers in autoload
        //or either define it in our controller but not :)

        /*
         * Get All Products
         */
        public function get_products(){ //CI has a convention of using underscore and not CamelCase
            //CI uses sth called 'ACTIVE RECORD' - shorthand way to deal with DB
            //$this->db->query('SELECT ..'); but with active record:
            $this->db->select('*');
            $this->db->from('products');
            $query = $this->db->get();
            return $query->result();    //return result rows which will be stored in $data array in controller
        }
        /*
         * Get Single Product
         */
        public function get_product_details($id){
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where('id', $id);    //it is coming from URL
            $query = $this->db->get();
            return $query->row();
        }

        /*
         * Get Categories
         */
        public function get_categories(){   //similar to get_products();
            $this->db->select('*');
            $this->db->from('categories');
            $query = $this->db->get();
            return $query->result();
        }

         /*
          * Get Most Popular Products
          * (we select orders but we join in products table
          * so that we can have things like product's title)
          */
        public function get_popular(){      //here we use allies (O-orders,P-products)
            $this->db->select('P.*, COUNT(O.product_id) as total'); //total - the number of instances for each product
            $this->db->from('orders AS O');
            $this->db->join('products AS P', 'O.product_id = P.id', 'INNER');
            $this->db->group_by('O.product_id');
            $this->db->order_by('total', 'desc');
            $query = $this->db->get();
            return $query->result();
            //order_by() - if u have product that has been order 5 times and one ordered once
            //ordered 5 times will be on top of the list
        }
    }