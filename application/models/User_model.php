<?php
// Dont forget to load our Models in autoload

class User_model extends CI_Model {
    public function register(){
        //we create data array equal to all POST fields we want to Insert in DB
        $data = array (
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password'))
        );
        //active record:
        $insert = $this->db->insert('users', $data);
        return $insert; //if true will return true
    }

    public function login($username, $password){
        //Validate
        $this->db->where('username', $username);
        $this->db->where('password', $password);

        $result = $this->db->get('users');
        if($result->num_rows() == 1){
            return $result->row(0)->id;     //we return user's id if he is logged in
        } else {
            return false;
        }

    }
}