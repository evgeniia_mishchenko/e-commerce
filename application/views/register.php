
<div class="row">
    <div class="col-md-12">
        <div class="breadcrumbs">
            <ul class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>">Home</a> <span class="divider"></span></li>
                <li class="active">My Account</li>
            </ul>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <h2 class="text-center">My Account</h2>
    </div>
</div>

<div class="row">

    <div class="col-md-3"></div>
    <div class="col-md-6">
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>

        <form class="loginbox form-horizontal" method="POST" action="<?php echo base_url(); ?>users/register">
            <p>Register</p>
            <div class="form-group">
                <label class="control-label col-md-4" for="firstname">First Name<span class="required">*</span></label>
                <div class="col-md-8">
                    <input name="first_name" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="lastname">Last Name<span class="required">*</span></label>
                <div class="col-md-8">
                    <input name="last_name" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="email">Email<span class="required">*</span></label>
                <div class="col-md-8">
                    <input name="email" type="text" id="email" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="username">Choose Username<span class="required">*</span></label>
                <div class="col-md-8">
                    <input name="username" type="text"  class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="password">Password<span class="required">*</span></label>
                <div class="col-md-8">
                    <input name="password" type="password" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="password2">Confirm Password<span class="required">*</span></label>
                <div class="col-md-8">
                    <input name="password2" type="password" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button class="btn btn-primary pull-right" name="submit" type="submit">Register</button>
                </div>
            </div>
        </form>

    </div>
    <div class="col-md-3"></div>
</div>