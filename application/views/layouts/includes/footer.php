</div>
<!-- End container -->



<div class="footer black">
    <div class="container">
        <!-- div class="arrow"><b class="caret"></b></div -->
        <div class="row">
            <div class="col-md-3">
                <div>
                    <h3>Information</h3>
                    <ul>
                        <li><a href="">About Us</a></li>
                        <li><a href="">Delivery Information</a></li>
                        <li><a href="">Privacy Policy</a></li>
                        <li><a href="">Terms & Conditions</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    <h3>Customer Service</h3>
                    <ul>
                        <li><a href="" class="ajax_right">Contact Us</a></li>
                        <li><a href="">Returns</a></li>
                        <li><a href="">Site Map</a></li>
                        <li><a href="">Shipping</a></li>
                    </ul>
                </div>
            </div>

            <!-- div class="col-md-3 twitter">
                <div class="row">
                    <h3>Follow us</h3>
                    <script type="text/javascript" src="js/twitterFetcher_v9_min.js"></script>
                    <ul id="twitter_update_list"><li class="col-md-2">Twitter feed loading</li></ul>
                    <script>twitterFetcher.fetch('256524641194098690', 'twitter_update_list', 2, true, true, false);</script>
                </div>
            </div-->
            <div class="col-md-3">
            </div>


            <div class="col-md-3 social">
                <div class="copy">Copyright &copy; 2015</div>
                <ul class="social-network">
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                    <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                    <li><a href=""><i class="fa fa-rss"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>

        </div>
    </div>
</div>
</div>

<!-- script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='js/jquery-1.10.2.min.js'><\/script>")</script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.flexslider-min.js"></script>  <!-- SLIDER - frontpage-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/cloud-zoom.1.0.3.js"></script>   <!-- ZOOM - product page-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>


</body>
</html>
