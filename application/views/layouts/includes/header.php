<!doctype html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-select.css">
  <link href='http://fonts.googleapis.com/css?family=Montserrat:200,300,400,600,700' rel='stylesheet' type='text/css'/>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:200,300,400,600,700' rel='stylesheet' type='text/css'/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/flexslider.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css">

  <!-- for base_url()- function to work first we need to load url helper. CI has a lot of helpers
      but to use them we need to load theme in autoload config file like this:
      $autoload['helper'] = array('url');
      and libraries as well
      ->

  <!--script type="text/javascript" src="https://getfirebug.com/firebug-lite-debug.js"></script-->
  <title>Sapphire</title>
</head>
<body>
<div class="page-container">
  <div class="header">
    <nav class="navbar container">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="<?php base_url(); ?>" class="navbar-brand">
          <img src="<?php echo base_url(); ?>assets/icons/logo.png" alt="Sapphire">Cars
        </a>
      </div>


      <div class="navbar-collapse collapse navbar-right">
        <ul class="nav navbar-nav">
            <li class="active"><a href="<?php base_url(); ?> ">Home</a></li>
            <li><a href="<?php echo base_url(); ?>users/login">Login</a></li>
            <li><a href="<?php echo base_url(); ?>users/register">Create Account</a></li>
            <li><a href="<?php echo base_url(); ?>cart">Cart</a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right cart">
          <li class="dropdown">
            <a href="" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>assets/icons/cart.png" alt="cart">
                <span><?php echo $this->cart->total_items(); ?></span>
            </a>
            <div class="cart-info dropdown-menu">
              <form action="" method="POST">
                <table class="table">
                  <thead>
                  </thead>
                  <tbody>
                  <?php $i = 1; ?>
                  <?php foreach($this->cart->contents() as $items) : ?>
                      <input type="hidden" name="<?php echo $i.'[rowid]'; ?>" value="<?php echo $items['rowid']; ?>">
                      <input type="hidden" name="<?php echo $i.'[qty]'; ?>" value="<?php echo $items['qty']; ?>">
                      <tr>
                          <td class="image"><img src="<?php echo base_url(); ?>assets/products/<?php echo $items['image']; ?>" alt="IMAGE" class="img-responsive"></td>
                          <td class="name"><a href=""><?php echo strtolower($items['name']); ?></a></td>
                          <td class="quantity">x&nbsp;<?php echo $items['qty']; ?> </td>
                          <td class="total">$<?php echo $this->cart->format_number($items['price']); ?></td>
                      </tr>
                      <?php $i++; ?>
                  <?php endforeach; ?>
                  </tbody>
                </table>
                <div class="cart-total">
                  <table>
                      <tbody>
                          <tr>
                              <td><b>Sub-Total: </b></td>
                              <td>$400.00</td>
                          </tr>
                          <tr>
                              <td><b>Total: </b></td>
                              <td> $<?php echo $this->cart->format_number($this->cart->total()); ?></td>
                          </tr>
                      </tbody>
                  </table>
                  <div class="checkout">
                    <a href="cart.html" class="ajax_right">View Cart</a> |
                    <a href="checkout.html" class="ajax_right">Checkout</a>
                  </div>
                </div>
              </form>
            </div>
          </li>
          <li>
                <?php if($this->session->userdata('logged_in')) : ?>
                    <form method="POST" action="<?php echo base_url(); ?>users/logout">
                        <button name="submit" class="btn btn-primary" type="submit">Logout</button>
                    </form>
                <?php endif; ?>
          </li>
        </ul>

        <form action="/" class="navbar-form navbar-search navbar-right" role="search">
          <div class="input-group">
            <input type="text" name="search" placeholder="Search" class="search-query col-md-2"><button type="submit" class="btn btn-default icon-search"><img src="<?php echo base_url(); ?>assets/icons/search.png"></button>
          </div>
        </form>


      </div><!-- /.navbar-collapse -->
    </nav>
  </div>

  <div class="container">