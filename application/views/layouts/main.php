<!-- Here we are loading views inside a view -->
<?php $this->load->view('layouts/includes/header'); ?>

 <!-- Display Main Content-->
    <?php $this->load->view($main_content); ?>
<!-- %main_content is defined inside of our controller-->
<?php $this->load->view('layouts/includes/footer'); ?>

<!-- And that's all for main.php-->
<!-- If we have the same sidebar for all pages we can put it into separate sidebar.php in includes folder and include in header.php -->
