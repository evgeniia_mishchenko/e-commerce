<!--we don't have to include header in every single view
    inside of View folder we have layouts folder inside which is main.php(there are header/footer)
    and includes folder where there are all includes-->
<div class="row">
    <div class="col-md-12 slideshow">
        <div>
            <ul class="slides">
                <li>
                    <a href="">
                        <img src="<?php echo base_url(); ?>assets/slider/porsche.jpeg" alt="Banner 1" />
                    </a>
                </li>
                <li>
                    <a href="">
                        <img src="<?php echo base_url(); ?>assets/slider/bmw.jpg" alt="Banner2" />
                    </a>
                </li>
                <li>
                    <a href="">
                        <img src="<?php echo base_url(); ?>assets/slider/engine.jpg" alt="Banner" />
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!--
        <div class="row banners">
            <div class="col-md-4">
                <div class="banner">
                    <h2>Free delivery</h2>
                </div>
            </div>

            <div class="col-md-4">
                <div class="banner">
                </div>
            </div>


            <div class="col-md-4">
                <div class="banner">
                </div>
            </div>
        </div>
        -->

<div class="row">
    <div class="col-md-3 left-menu">
        <div class="">
            <h3>Categories</h3>
            <ul>
                <?php foreach( get_categories_h() as $category) : ?>
                    <li><a href="<?php base_url(); ?>products/category/<?php echo $category->id; ?>"><?php echo $category->name; ?></a></li>
                <?php endforeach; ?>
                <!--<li class="active">-->
            </ul>
            <h3>Most popular</h3><!-- From orders table. Use helper function -->
            <ul>
                <?php foreach( get_popular_h() as $popular) : ?>
                    <li><a href="<?php echo base_url(); ?>products/details/<?php echo $popular->id; ?>"><?php echo strtolower($popular->title); ?></a></li>
                <?php endforeach; ?>
            </ul>

        </div>


        <div class="options">
            <select class="selectpicker"  data-width="150px">
                <option>US Dollars</option>
                <option>Euro</option>
                <option>Pounds</option>
            </select>
            <section id="content">
                <div id="exchangerates"></div>
            </section>
        </div>
    </div>

    <div class="col-md-9">
        <div class="row">
            <?php foreach($products as $product) : ?>
                <div class="col-md-4">
                    <div class="product">
                        <a href="<?php echo base_url(); ?>products/details/<?php echo $product->id; ?>"><img alt="dress1home" src="<?php echo base_url(); ?>assets/products/<?php echo $product->image; ?>"></a>
                        <div class="name">
                            <a href="<?php echo base_url(); ?>products/details/<?php echo $product->id; ?>"><?php echo $product->title; ?></a>
                        </div>
                        <div class="price">
                            <p>$<?php echo $product->price; ?></p>
                        </div>
                        <form method="POST" action="<?php echo base_url(); ?>cart/add">
                            <span>Qty: </span>
                            <input type="text" name="qty" value="1" style="width: 25px;height: 20px;border: 1px solid #eee;box-shadow: none;" />
                            <input type="hidden" name="item_number" value="<?php echo $product->id; ?>" />      <!-- input called item_number is equal to product.id-->
                            <input type="hidden" name="price" value="<?php echo $product->price; ?>" />
                            <input type="hidden" name="title" value="<?php echo $product->title; ?>" />
                            <input type="hidden" name="image" value="<?php echo $product->image; ?>" />
                            <button type="submit" style="background: white;border: none;"><img src="<?php echo base_url(); ?>assets/icons/cart.png"></button>
                        </form>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>



        <div class="row">
            <div class="col-md-12">

                <div class="newsletter clearfix">
                    <h3>Newsletter</h3>
                    <div>
                        <input type="text" name="email" class="email">
                        <input type="submit" value="Subscribe" class="btn btn-primary">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
