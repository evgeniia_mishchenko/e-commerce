
<div class="row">
    <div class="col-md-12">
        <div class="breadcrumbs">
            <ul class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>">Home</a> <span class="divider"></span></li>
                <li class="active">My Account</li>
            </ul>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <h2 class="text-center">My Account</h2>
    </div>
</div>

<div class="row">
    <div class="col-md-3"></div>

    <div class="col-md-6">
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>

        <?php if($this->session->flashdata('fail_login')) : ?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('fail_login') ; ?>
            </div>
        <?php endif; ?>
        <form class="loginbox form-horizontal" method="POST" action="<?php echo base_url(); ?>users/login">
            <p>Login</p>
            <div class="form-group">
                <label class="control-label col-md-4" for="username">Username<span class="required">*</span></label>
                <div class="col-md-8">
                    <input name="username" type="text" class="form-control" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="password">Password<span class="required">*</span></label>
                <div class="col-md-8">
                    <input name="password" type="password"  class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button name="submit" class="btn btn-primary pull-right" type="submit">Login</button>
                    <a href="forgot-password.html">Lost Password?</a>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-3"></div>
</div>