<div class="row">
    <div class="col-md-12">
        <div class="breadcrumbs">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a> <span class="divider"></span></li>
                <li class="active">Shopping Cart</li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h2>Shopping Cart</h2>
    </div>
</div>
<?php if($this->cart->contents()) :  ?>
    <form method="POST" action="cart/process">
        <div class="row">
            <div class="col-md-12">
                <div class="cart-info">

                    <table class="table">
                        <thead>
                        <tr>
                            <th class="image">Image</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th class="total">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0; ?>
                        <?php foreach($this->cart->contents() as $items) : ?>
                        <tr>
                            <td class="image"><img alt="IMAGE" src="<?php echo base_url(); ?>assets/products/<?php echo $items['image']; ?>"></td>
                            <td class="name"><a href="<?php echo base_url(); ?>products/details/<?php echo $items['id']; ?>"><?php echo $items['name']; ?></a></td>
                            <td>$<?php echo $this->cart->format_number($items['price']); ?></td>
                            <td class="quantity">
                                <input type="text" size="1" value="<?php echo $items['qty']; ?>" name="quantity">
                                <input type="image" title="Update" alt="Update" src="<?php base_url(); ?>assets/icons/update.png">
                                <input type="image" title="Remove" alt="Remove" src="<?php base_url(); ?>assets/icons/remove.png">
                            </td>
                            <?php echo '<input type="hidden" name="item_name['.$i.']" value="'.$items['name'].'" />'; ?>
                            <?php echo '<input type="hidden" name="item_code['.$i.']" value="'.$items['id'].'" />'; ?>
                            <?php echo '<input type="hidden" name="item_qty['.$i.']" value="'.$items['qty'].'" />'; ?>
                            <td class="total">$</td>
                        </tr>
                        <?php $i++; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 col-sm-offset-8">
                <div class="cart-totals">
                    <table class="table">
                        <tr>
                            <th>Cart Subtotal</th>
                            <td>$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
                        </tr>
                        <tr>
                            <th>Shipping</th>
                            <td><?php echo $this->config->item('shipping'); ?></td>
                        </tr>
                        <tr>
                            <th>Tax</th>
                            <td><?php echo $this->config->item('tax'); ?></td>
                        </tr>
                        <tr>
                            <th><span>Order Total</span></th>
                            <td>$360.00</td>
                        </tr>
                    </table>
                    <p>
                        <a class="btn btn-primary" href="<?php echo base_url(); ?>checkout">
                            Proceed to Checkout
                        </a>
                    </p>
                </div>
            </div>
        </div>

    </form>
<?php else : ?>
    <p>There are no items in your cart</p>
<?php endif; ?>