<ul class="breadcrumb prod">
    <li><a href="index.html">Home</a> <span class="divider"></span></li>
    <li class="active">Product</li>
</ul>

<div class="row product-info">
    <div class="col-md-6">

        <div class="image"><a class="cloud-zoom" rel="adjustX: 0, adjustY:0" id='zoom1' href="<?php echo base_url(); ?>assets/products/<?php echo $product->image; ?>" title="Nano"><img src="<?php echo base_url(); ?>assets/products/<?php echo $product->image; ?>" title="Nano" alt="Nano" id="image" /></a></div>
        <!--<div class="image-additional">
            <a title="Dress" rel="useZoom: 'zoom1', smallImage: 'products/dress1home.jpg'" class="cloud-zoom-gallery" href="products/Acura_NSX.jpg"><img alt="Dress" title="Dress" src="products/Acura_NSX.jpg"></a>
            <a title="Dress" rel="useZoom: 'zoom1', smallImage: 'products/dress5home.jpg'" class="cloud-zoom-gallery" href="products/dress5home.jpg"><img alt="Dress" title="Dress" src="products/dress5home.jpg"></a>
            <a title="Dress" rel="useZoom: 'zoom1', smallImage: 'products/dress6home.jpg'" class="cloud-zoom-gallery" href="products/dress6home.jpg"><img alt="Dress" title="Dress" src="products/dress6home.jpg"></a>
            <a title="Dress" rel="useZoom: 'zoom1', smallImage: 'products/dress4home.jpg'" class="cloud-zoom-gallery" href="products/dress4home.jpg"><img alt="Dress" title="Dress" src="products/dress4home.jpg"></a>
        </div>-->
    </div>
    <div class="col-md-6">
        <h1><?php echo $product->title; ?></h1>
        <div class="line"></div>
        <!--<ul>
            <li><span>Brand:</span> <a href="#">Shop Online</a></li>
            <li><span>Product Code:</span> Product 001</li>
            <li><span>Availability: </span>In Stock</li>
        </ul>-->
        <div class="price">
            Price <strong>$<?php echo $product->price; ?></strong>
        </div>

        <!--<select class="selectpicker" data-width="150px">
            <option>Red</option>
            <option>Blue</option>
            <option>Green</option>
        </select>
        <select class="selectpicker" data-width="150px">
            <option>180 cm</option>
            <option>160 cm</option>
            <option>140 cm</option>
        </select>

        <div class="line"></div>-->

        <form class="form-inline" method="POST" action="<?php base_url(); ?>cart/add/<?php echo $product->id; ?>">
            <input type="hidden" name="item_number" value="<?php echo $product->id; ?>" />
            <input type="hidden" name="price" value="<?php echo $product->price; ?>" />
            <input type="hidden" name="title" value="<?php echo $product->title; ?>" />
            <button name="buy_submit" class="btn btn-primary" type="submit">Add to Cart</button>
            <label>Qty:</label> <input name="qty" value="1" type="text" placeholder="1" class="col-md-1">
        </form>

        <div class="tabs">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#home">Description</a></li>
                <li><a href="#profile">Specification</a></li>
                <li><a href="#messages">Reviews</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home"><?php echo $product->description; ?> </div>
                <div class="tab-pane" id="profile">
                    <table class="table specs">
                        <tr>
                            <th>...</th>
                            <th>...</th>
                            <th>...</th>
                        </tr>
                        <tr>
                            <td>..</td>
                            <td>..</td>
                            <td>..</td>
                        </tr>
                        <tr>
                            <th>...</th>
                            <th>...</th>
                            <th>...</th>
                        </tr>
                        <tr>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tr>
                    </table>
                </div>
                <div class="tab-pane" id="messages">
                    <!--<p>There are no reviews yet, would you like to <a href="#review_btn">submit yours?</a></p>
                    <h3>Be the first to review 	&ldquo;Blue Dress&rdquo; </h3>
                    <form>
                        <fieldset>
                            <label>Name<span class="required">*</span></label>
                            <input type="text" placeholder="Name">
                            <label>Email<span class="required">*</span></label>
                            <input type="text" placeholder="Email">
                        </fieldset>
                        <br>
                    </form>
                    <label>Your Review<span class="required">*</span></label>
                    <textarea rows="3"></textarea>
                    <p id="review_btn">
                        <button class="btn btn-default" type="button">Submit Review</button>
                    </p>-->
                </div>
            </div>
        </div>
    </div>
</div>