<?php
/**
 *  Helpers are not classes, its just group of functions
 * so we dont have to worry about encapsulation(we dont need public or private)
 * helper -> model -> DB
 */

/*
 * Get Categories( _h - to know its a helper function )
 */

function get_categories_h(){
    //since we are not in the controller/model/library to deal with DB
    //or model rather u dont directly deal with DB u actually connect to the model
    //but u cant say like: $this->Product_model; we have to create an instance
    //because we arent directly in the controller:
    $CI = get_instance();   //$CI instead of $this to access the model;
    $categories = $CI->Product_model->get_categories();
    return $categories;
}

function get_popular_h(){
    $CI = get_instance();
    $CI->load->model('Product_model');
    $popular_products = $CI->Product_model->get_popular();
    return $popular_products;
}
