$(window).on('load', function ()
{
    $('.selectpicker').selectpicker({});
});

    //for Slider on frontpage
$(document).ready( function(){

    $('.slideshow > div').flexslider({
        animation:"slide",
        easing:"",
        direction:"horizontal",
        startAt:0,
        initDelay:0,
        slideshowSpeed:7000,
        animationSpeed:600,
        prevText:"Previous",
        nextText:"Next",
        pauseText:"Pause",
        playText:"Play",
        pausePlay:false,
        controlNav:true,
        slideshow:true,
        animationLoop:true,
        randomize:false,
        smoothHeight:false,
        useCSS:true,
        pauseOnHover:true,
        pauseOnAction:true,
        touch:true,
        video:false,
        mousewheel:false,
        keyboard:false
    });
});

    //for Zoom on product page
$.fn.CloudZoom.defaults = {
    zoomWidth:"auto",
    zoomHeight:"auto",
    position:"inside",
    adjustX:0,
    adjustY:0,
    adjustY:"",
    tintOpacity:0.5,
    lensOpacity:0.5,
    titleOpacity:0.5,
    smoothMove:3,
    showTitle:false};

//for Tabs on the bottom of product page
jQuery(document).ready(function()
{
    $('#myTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })
});

//Exchange Rates
// NOTE: This example will not work locally in Chrome / IE because of cross-domain restrictions.
// You can try it out on the website for the book http://javascriptbook.com/code
// or run it on your own server.

$('#exchangerates').append('<div id="rates"></div><div id="reload"></div>');
function loadRates() {
    $.getJSON('rates.json')
        .done( function(data){                                 // SERVER RETURNS DATA
            var d = new Date();                                  // Create date object
            var hrs = d.getHours();                              // Get hours
            var mins = d.getMinutes();                           // Get mins
            var msg = '<h2>Exchange Rates</h2>';                 // Start message
            $.each(data, function(key, val) {                    // Add each rate
                msg += '<div class="' + key + '">' + key + ': ' + val + '</div>';
            });
            msg += '<br>Last update: ' + hrs + ':' + mins + '<br>'; // Show update time
            $('#rates').html(msg);                               // Add rates to page
        }).fail( function() {                                  // THERE IS AN ERROR
            $('#rates').text('Sorry, we cannot load rates.');   // Show error message
        }).always( function() {                                // ALWAYS RUNS
            var reload = '<a id="refresh" href="#">';           // Add refresh link
            reload += '<img src="assets/icons/refresh.png" alt="refresh" /></a>';
            $('#reload').html(reload);                          // Add refresh link
            $('#refresh').on('click', function(e) {             // Add click handler
                e.preventDefault();                               // Stop link
                loadRates();                                      // Call loadRates()
            });
        });
}

loadRates();                                            // Call loadRates()

// When working locally in Firefox, you may see an error saying that the JSON is not well-formed.
// This is because Firefox is not reading the correct MIME type (and it can safely be ignored).
// If you get it on a server, you may need to se the MIME type for JSON on the server (application/JSON).

